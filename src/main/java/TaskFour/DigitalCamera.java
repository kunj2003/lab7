/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package TaskFour;

/**
 *
 * @author Kunj Prajapati
 */
 public abstract class DigitalCamera 
{
     String Make;
     String Model;
     double MegaPixels;

     public DigitalCamera(String Make, String Model, double MegaPixels) {
        this.Make = Make;
        this.Model = Model;
        this.MegaPixels = MegaPixels;
    }
     
    public String getMake() {
        return Make;
    }

    public String getModel() {
        return Model;
    }

    public double getMegaPixels() {
        return MegaPixels;
    }

   
    
   
     public abstract String describeCamera();
    
    
}
 
