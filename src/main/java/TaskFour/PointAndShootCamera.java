/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package TaskFour;

/**
 *
 * @author Kunj Prajapati
 */
class PointAndShootCamera extends DigitalCamera
{
     int InternalMemorySize;
    
     public PointAndShootCamera(String Make, String Model, double MegaPixels,int InternalMemorySize ) 
    {
       super(Make,Model,MegaPixels);
       this.InternalMemorySize = InternalMemorySize;
    }
     
    public int getInternalMemorySize() 
    {
        return InternalMemorySize;
    }
    Public String describeCamera()
    {
        return "Make is : " +super.getMake() + "\nModel is : " +super.getModel() + "\nMegapixels is : " +super.getMegaPixels()+ "\nInternal Memory size is :  " +this.InternalMemorySize+"GB";
        
    }
            
}
