/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package TaskFour;

/**
 *
 * @author Kunj Prajapati
 */
class PhoneCamera extends DigitalCamera
{
     int ExternalMemorySize;
    
     public Phonecamera(String Make, String Model, double MegaPixels,int ExternalMemorySize ) 
    {
       super(Make,Model,MegaPixels);
       this.ExternalMemorySize = ExternalMemorySize;
    }
     
    public int getExternalMemorySize() 
    {
        return this.ExternalMemorySize;
    }
    
   String describeCamera()
    {
        return "Make is : " +super.getMake() + "Model is : " +super.getModel() + "Megapixels is : " +super.getMegaPixels()+ "Internal Memory size is :  " +this.ExternalMemorySize+"GB";
    }

   
            
}