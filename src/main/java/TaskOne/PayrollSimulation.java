/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package TaskOne;

/**
 *
 * @author Kunj Prajapati
 */
public class PayrollSimulation {
    
    public static void main(String[] args) {
        
        Employee emp1=new Employee("Kunj", 20.00, 15.25);
        Manager man1 = new Manager("Mit", 40.00, 19.00);

        System.out.println(" Kunj gets paycheque of $ " + emp1.calculatePay());
        System.out.println(" Mit gets paycheque of $ " + man1.calculatePaym());

    }
}
