/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package TaskOne;

/**
 *
 * @author Kunj Prajapati
 */
public class Manager extends Employee{ 

    private double bonus=150;
 
    public Manager(String name, double hours, double wage) {
        super(name, hours, wage);
    }

    public double calculatePaym(){
        return (getNofHours()*getWages()) + bonus;
    }


}
