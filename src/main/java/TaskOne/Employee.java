/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package TaskOne;

/**
 *
 * Author Kunj Prajapati
 */
public class Employee {

    private String name;
    private double nOfHours;
    private double wage;

    public Employee( String name , double nOfHours,double wage) {
        this.nOfHours=nOfHours;
        this.name=name;
        this.wage=wage;

    }

    public String getName () {
        return name;

    }

    public double getNofHours () {
        return nOfHours;
        
    }

    public double getWages () {
        return wage;
        
    }
    public double calculatePay() {
        return (getNofHours()*getWages());
    }


    
}
