/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package TaskThree;

/**
 *
 * @author Kunj Prajapati
 */
public class ShapeSimulation 
{
    public static void main(String[]args)
    {
        Shape s1 = new Circle("red",8);
        Shape s2 = new Rectangle("yellow",6,6);
        
        System.out.println(s1.toString());
        System.out.println(s2.toString());
        
        
    }
    
}
