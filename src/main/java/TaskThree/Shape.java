/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package TaskThree;

/**
 *
 * @author Kunj Prajapati
 */
public abstract class Shape
{
    private String colour;
    
    //abstract method
    abstract double area();
    public abstract String toString();
    
    public Shape(String colour)
    {
        this.colour = colour;  
    }

    public String getColour() {
        return colour;
    } 
}
