/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package TaskThree;

/**
 *
 * @author Kunj Prajapati
 */
public class Rectangle extends Shape
{
    double length,width;
    
    public Rectangle(String colour, double length, double width)
    {
        super(colour);
        this.length = length;
        this.width = width;
    }    
    double area()
    {
        return length * width;
    }
    public String toString()
    {
         return "Rectangle is = "+ super.getColour() +" and area of Rectangle is : " +area();
    }
    
}
