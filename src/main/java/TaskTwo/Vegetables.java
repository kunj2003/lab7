/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package TaskTwo;

/**
 *
 * @author Kunj Prajapati
 */


public abstract class  Vegetables{

    private String color;
    private double size;

    public Vegetables(String color ,double size ) {
        this.color=color;
        this.size=size;
    }

    public String getColor(){
        return color;
    }

    public double getSize(){
        return size;
    }

    public abstract void isRipe();

   
}
