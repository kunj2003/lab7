/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package TaskTwo;

/**
 *
 * @author Kunj Prajapati
 */
import java.util.ArrayList;

public class VegetableSimulation {

    public static void main(String[] args) {
        
        ArrayList<Carrot> list1 = new ArrayList<Carrot>();
        ArrayList<Beet> list2= new ArrayList<Beet>();

        list1.add(new Carrot("red", 2.0));
        list1.add(new Carrot("Orange", 1.5));

        list2.add(new Beet("red", 2.0));
        list2.add(new Beet("Orange", 1.5));

        list1.get(0).isRipe();
        list1.get(1).isRipe();

        list2.get(0).isRipe();
        list2.get(1).isRipe();
        


    }
    
}

