/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package TaskTwo;

/**
 *
 * @author Kunj Prajapati
 */
public class Carrot extends Vegetables {

    public int c;

    public Carrot(String c, double s) {
        super(c, s);
    }

    public void isRipe() {
        if(getColor().equalsIgnoreCase("orange") && getSize()==1.5) {
            System.out.println("carrot is ripe");
        }
    
} }
