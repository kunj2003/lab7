/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package TaskTwo;

/**
 *
 * @author Kunj Prajapati
 */
public class Beet extends Vegetables {
    public int c;

    public Beet(String c, double s) {
        super(c, s);
    }

    public void isRipe() {
        if(getColor().equalsIgnoreCase("Red") && getSize()==2) {
            System.out.println("Beet is ripe");
        }
    
    }
}
